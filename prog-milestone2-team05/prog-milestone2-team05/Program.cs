﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team05
{
    class Program
    {
        static void Main(string[] args)
        {
            //Team 05 Milestone 2
            //Ashton, Kelly and Josiah

            //Menu
            var userSelect = "";

            while (userSelect != "4")
            {
                Console.Clear();

                Console.WriteLine("Welcome to Team 05's Program!");
                Console.WriteLine("");
                Console.WriteLine("Please select a Task from the menu:");
                Console.WriteLine("1. Task 1 - Date Calculator");
                Console.WriteLine("2. Task 2 - Grade Average Calculator");
                Console.WriteLine("3. Task 3 - Random Number Game");
                Console.WriteLine("4. Exit the Program");
                Console.WriteLine("");
                Console.WriteLine("Enter the number of the Task you want and press Enter.");

                userSelect = Console.ReadLine();

                if (userSelect == "1")
                {
                    DateCalculator();
                }
                else if (userSelect == "2")
                {
                    GradeCalc();
                }
                else if (userSelect == "3")
                {
                    random();
                }
            }                      
        }

        //Task 1 (Ashton) - Date Calculator
        
        //Method DateCalculator used to make sub-menu for user to select which part of the Date Calculator they want to use.
        static void DateCalculator()
        {
            var todayDate = DateTime.Now;
            var dateCalcSelect = "";

            while (dateCalcSelect != "3")
            {
                Console.Clear();

                Console.WriteLine("Welcome to the Date Calculator!");
                Console.WriteLine("");
                Console.WriteLine("Please select the date calculation you want to do, or return to the main menu:");
                Console.WriteLine("1. Calculate your age in days");
                Console.WriteLine("2. Calculate the number of days in a number of years");
                Console.WriteLine("3. Return to the Main Menu");

                dateCalcSelect = Console.ReadLine();

                if (dateCalcSelect == "1")
                {
                    DaysOld(todayDate);
                }
                else if (dateCalcSelect == "2")
                {
                    DaysInYears(todayDate);
                }
            }            
        }
        
        //Method DaysOld is used to calculate how many days old a user is.
        static void DaysOld(DateTime todayDate)
        {

            var userBirthday = DateTime.Now;
            double ageDays = 0;

            Console.WriteLine("");
            Console.WriteLine("You have chosen option 1!");
            Console.WriteLine("Please enter your date of birth (DD/MM/YYYY):");
            if (DateTime.TryParse(Console.ReadLine(),out userBirthday))
            {
                ageDays = Math.Round((todayDate - userBirthday).TotalDays);
                Console.WriteLine("You are " + ageDays + " days old!");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine("Please enter your date of birth in the format DD/MM/YYYY");
                Console.ReadLine();
            }
        }

        //Method DaysInYears is used to work out how many days there are in a number of years.
        static void DaysInYears(DateTime todayDate)
        {
            var numYears = 0;
            var futureDate = DateTime.Now;
            double totalDays = 0;

            Console.WriteLine("");
            Console.WriteLine("You have chosen option 2!");
            Console.WriteLine("Please enter the number of years you'd like to calculate:");
            if (int.TryParse(Console.ReadLine(), out numYears))
            {
                futureDate = todayDate.AddYears(numYears);
                totalDays = Math.Round((futureDate - todayDate).TotalDays);
                Console.WriteLine("There are " + totalDays + " days in the next " + numYears + " years.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine("Please enter a valid number of years.");
                Console.ReadLine();
            }
        }

        
        //Task 3 (Kelly) - Random Number Game
        
        //Random Number Game
        static void random()
        {
            Random r = new Random(); //for random nummbers
            int count = 5;
            int i = 0;
            int score = 0;
            var scores = new List<int> { };
            int value = r.Next(1, 5); //to set the range

            Console.Clear();

            Console.WriteLine("Welcome to the random number game");
            Console.WriteLine("The range of number is between 1 and 5");
            while (i < count)
            {
                Console.WriteLine("");
                Console.WriteLine("Please guess what number you think it is");
                var guess = int.Parse(Console.ReadLine());
                if (guess == value)
                {
                    Console.WriteLine("You are right!");
                    score++;
                }
                else
                {
                    Console.WriteLine("Sorry, that is not correct");
                }
                i++;
            }

            Console.WriteLine("");
            Console.Clear();
            Console.WriteLine("You scored " + score + " points");
            scores.Add(score);
            end(scores);

            //End of Class
        }

        //For the end of random number game
        static void end(List<int> scores)
        {
            Console.WriteLine("your previous scores are as followed");

            foreach (var x in scores)
            {
                Console.WriteLine(x);
            }

            Console.WriteLine("");
            Console.WriteLine("Would you like to play again? Y/N");
            string line = Console.ReadLine();

            if (line == "Y")
            {
                random();
            }
            else
            {

            }
            // End of the program

        }

        
        //Task 2 (Josiah) - Grade Calculator

        //Gets user info to use in other methods
        public static void GradeCalc()
        {
            var level = "5";
            var paper = "";
            var mark = 0.00;
            var id = "";
            var grades = new List<Tuple<string, double>>();
            int i = 0;
            var length = 0;

            //intro
            Console.Clear();
            do
            {
                Console.WriteLine("Welcome to the Grade Calculator");
                Console.WriteLine("Are you level 5 or level 6?");
                level = Console.ReadLine();
            } while (level != "5" && level != "6");


            Console.WriteLine("");
            Console.WriteLine("What is your student ID?");
            id = Console.ReadLine();


            //Level 5
            //if level 5 asks for input
            if (level == "5")
            {

                for (i = 1; i < 5; i++)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Please enter your paper code " + i + "");
                    paper = Console.ReadLine();
                    Console.WriteLine("Now enter your mark for that paper.");
                    mark = int.Parse(Console.ReadLine());
                    grades.Add(Tuple.Create(paper, mark));
                }
                length = grades.Count();
                GradeMenu(grades, id, level, length);
            }

            //Level 6
            //if level 6 asks for input
            else if (level == "6")
            {

                for (i = 1; i < 4; i++)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Please enter your paper code " + i + "");
                    paper = Console.ReadLine();
                    Console.WriteLine("Now enter your mark for that paper.");
                    mark = int.Parse(Console.ReadLine());
                    grades.Add(Tuple.Create(paper, mark));
                }
                length = grades.Count();
                GradeMenu(grades, id, level, length);
            }
        }

        //grade percentages
        public static void PrintResult(List<Tuple<string, double>> grades, string id, string level, int length)
        {
            var i = 0;
            var letterGrade = "";
            Console.Clear();
            Console.WriteLine("These are the marks for student #" + id + " at level " + level + "");

            length = grades.Count();
            for (i = 0; i < length; i++)
            {
                if (grades[i].Item2 <= 39)
                {
                    letterGrade = "E";
                }
                else if (grades[i].Item2 <= 49 && grades[i].Item2 >= 40)
                {
                    letterGrade = "D";
                }
                else if (grades[i].Item2 <= 54 && grades[i].Item2 >= 50)
                {
                    letterGrade = "C";
                }
                else if (grades[i].Item2 <= 59 && grades[i].Item2 >= 55)
                {
                    letterGrade = "C+";
                }
                else if (grades[i].Item2 <= 64 && grades[i].Item2 >= 60)
                {
                    letterGrade = "B-";
                }
                else if (grades[i].Item2 <= 69 && grades[i].Item2 >= 65)
                {
                    letterGrade = "B";
                }
                else if (grades[i].Item2 <= 74 && grades[i].Item2 >= 70)
                {
                    letterGrade = "B+";
                }
                else if (grades[i].Item2 <= 79 && grades[i].Item2 >= 75)
                {
                    letterGrade = "A-";
                }
                else if (grades[i].Item2 <= 84 && grades[i].Item2 >= 80)
                {
                    letterGrade = "A";
                }
                else if (grades[i].Item2 <= 100 && grades[i].Item2 >= 85)
                {
                    letterGrade = "A+";
                }
                Console.WriteLine("");
                Console.WriteLine("Paper #" + grades[i].Item1 + "");
                Console.WriteLine("Grade mark " + grades[i].Item2 + ", " + letterGrade + "");

            }
        }

        //grade average
        public static void Average(List<Tuple<string, double>> grades, int length)
        {
            var totalMarks = 0.00;
            var i = 0;
            var gradeAverage = 0.00;

            for (i = 0; i < length; i++)
            {
                totalMarks = totalMarks + grades[i].Item2;

            }
            gradeAverage = totalMarks / length;

            if (gradeAverage >= 50)
            {
                Console.WriteLine("Your average grade is " + gradeAverage + " which means you passed.");
            }
            else
            {
                Console.WriteLine("Your average grade is " + gradeAverage + " which means you failed.");
            }
        }

        //says what papers are A+
        public static void APapers(List<Tuple<string, double>> grades, int length)
        {
            var i = 0;

            Console.WriteLine("These are the papers you got A+ in:");
            for (i = 0; i < length; i++)
            {
                if (grades[i].Item2 >= 85)
                {
                    Console.WriteLine("" + grades[i].Item1 + "");
                }
            }
        }

        //grade calc sub menu
        public static void GradeMenu(List<Tuple<string, double>> grades, string id, string level, int length)
        {
            var gradeSelect = "";

            Console.Clear();

            while (gradeSelect != "4")
            {
                Console.WriteLine("");
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1. See all your marks.");
                Console.WriteLine("2. See your average mark.");
                Console.WriteLine("3. See which papers got A+");
                Console.WriteLine("4. Go back to main menu");
                Console.WriteLine("");

                gradeSelect = Console.ReadLine();

                if (gradeSelect == "1")
                {
                    PrintResult(grades, id, level, length);
                }
                else if (gradeSelect == "2")
                {
                    Average(grades, length);
                }
                else if (gradeSelect == "3")
                {
                    APapers(grades, length);
                }
            }
        }
    }
}
